<?php

// Register our widgets
add_action( 'widgets_init', function() {

	// Widget that prints tweets
	register_widget( 'SA_Candid_Career' );

});

// A widget for Candid Career.
class SA_Candid_Career extends WP_Widget {

	// Sets up the widgets name, etc
	public function __construct() {
		
		parent::__construct(
			'sa-candid-career-widget',
			'Candid Career',
			array(
				'description' 	=> 'Displays Candid Career widget',
				'classname'		=> 'sa-candid-career-widget',
				)
		);
		
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		echo $args['before_widget'];
		?><iframe class="candid-career-iframe" scrolling="no" frameborder="0" src="https://www.candidcareer.com/widget/widget4.php?uid=2155" marginheight="0" marginwidth="0"></iframe><?php
		echo $args['after_widget'];
		
	}

}