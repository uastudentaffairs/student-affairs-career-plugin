<?php

/**
 * Plugin Name:       Student Affairs - Career Center
 * Plugin URI:        https://sa.ua.edu
 * Description:       This WordPress plugin is intended solely for The University of Alabama Division of Student Affairs.
 * Version:           1.0
 * Author:            UA Division of Student Affairs - Rachel Carden
 * Author URI:        https://sa.ua.edu
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Add files
require_once plugin_dir_path( __FILE__ ) . 'includes/widgets.php';

// Internationalization
// @TODO Add language files
/*add_action( 'init', 'ua_sa_career_textdomain' );
function ua_sa_career_textdomain() {
	load_plugin_textdomain( 'ua-sa-career', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}*/

// Runs on install
register_activation_hook( __FILE__, 'ua_sa_career_install' );
function ua_sa_career_install() {

	// Register the custom post type
	ua_sa_career_register_cpt();

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Runs when the plugin is upgraded
// @TODO test to see if this only runs when running a bulk upgrade
add_action( 'upgrader_process_complete', 'ua_sa_career_upgrader_process_complete', 1, 2 );
function ua_sa_career_upgrader_process_complete( $upgrader, $upgrade_info ) {

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Add styles and scripts
add_action( 'wp_enqueue_scripts', function () {

	// Get the plugin CSS directory
	$sa_career_dir = plugin_dir_url( __FILE__ ) . 'css/';

	// Enqueue our main styles
	wp_enqueue_style( 'sa-career', $sa_career_dir . 'sa-career.css', array( 'sa-child' ), false );

}, 30 );

// Filter the presentation request form
add_filter( 'gform_field_choice_markup_pre_render_1', function( $choice_markup, $choice, $field, $value ) {
	global $career_optgroups;

	// Add <optgroup>s
	switch( $choice[ 'value' ] ) {

		// Add optgroup before
		case 'Overview of Services, Resources and Events':
			if ( ! ( isset( $career_optgroups['overview'] ) && true == $career_optgroups['overview'] ) ) {
				$career_optgroups['overview'] = true;
				$choice_markup                  = '<optgroup label="Career Center Services">' . $choice_markup;
			}
			break;

		case 'Self Assessment for Career Planning':
			if ( ! ( isset( $career_optgroups['self'] ) && true == $career_optgroups['self'] ) ) {
				$career_optgroups['self'] = true;
				$choice_markup                  = '</optgroup><optgroup label="Career Planning">' . $choice_markup;
			}
			break;

		case 'Getting an Internship':
			if ( ! ( isset( $career_optgroups['internship'] ) && true == $career_optgroups['internship'] ) ) {
				$career_optgroups['internship'] = true;
				$choice_markup              = '</optgroup><optgroup label="Experiential Learning (Internships, etc.)">' . $choice_markup;
			}
			break;

		case 'Resumes':
			if ( ! ( isset( $career_optgroups['resumes'] ) && true == $career_optgroups['resumes'] ) ) {
				$career_optgroups['resumes'] = true;
				$choice_markup                    = '</optgroup><optgroup label="Job Search Tools">' . $choice_markup;
			}
			break;

		case 'Starting Your Job Search':
			if ( ! ( isset( $career_optgroups['search'] ) && true == $career_optgroups['search'] ) ) {
				$career_optgroups['search'] = true;
				$choice_markup                 = '</optgroup><optgroup label="Job Search Strategies">' . $choice_markup;
			}
			break;

		case 'Dress for Success':
			if ( ! ( isset( $career_optgroups['dress'] ) && true == $career_optgroups['dress'] ) ) {
				$career_optgroups['dress'] = true;
				$choice_markup                = '</optgroup><optgroup label="Professional Development">' . $choice_markup;
			}
			break;

	}

	return $choice_markup;
}, 100, 4 );

// Register Career Center custom post types
add_action( 'init', 'ua_sa_career_register_cpt' );
function ua_sa_career_register_cpt() {

	// Register majors CPT
	register_post_type( 'majors', array(
		'labels' => array(
			'name'               => _x( 'Majors', 'post type general name', 'ua-sa-career' ),
			'singular_name'      => _x( 'Major', 'post type singular name', 'ua-sa-career' ),
			'menu_name'          => _x( 'Majors', 'admin menu', 'ua-sa-career' ),
			'name_admin_bar'     => _x( 'Majors', 'add new on admin bar', 'ua-sa-career' ),
			'add_new'            => _x( 'Add New', 'majors', 'ua-sa-career' ),
			'add_new_item'       => __( 'Add New Major', 'ua-sa-career' ),
			'new_item'           => __( 'New Major', 'ua-sa-career' ),
			'edit_item'          => __( 'Edit Major', 'ua-sa-career' ),
			'view_item'          => __( 'View Major', 'ua-sa-career' ),
			'all_items'          => __( 'All Majors', 'ua-sa-career' ),
			'search_items'       => __( 'Search Majors', 'ua-sa-career' ),
			'parent_item_colon'  => __( 'Parent Major:', 'ua-sa-career' ),
			'not_found'          => __( 'No majors found.', 'ua-sa-career' ),
			'not_found_in_trash' => __( 'No majors found in Trash.', 'ua-sa-career' )
		),
		'public'                => true,
		'publicly_queryable'    => false,
		'exclude_from_search'   => true,
		'show_in_nav_menus'     => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
		'menu_icon'             => 'dashicons-welcome-learn-more',
		'capabilities'          => array(
			'edit_post'         => 'edit_major',
			'read_post'         => 'read_major',
			'delete_post'       => 'delete_major',
			'edit_posts'        => 'edit_majors',
			'edit_others_posts' => 'edit_others_majors',
			'publish_posts'     => 'publish_majors',
			'read_private_posts'=> 'read_private_majors',
			'read'              => 'read',
			'delete_posts'      => 'delete_majors',
			'delete_private_posts' => 'delete_private_majors',
			'delete_published_posts' => 'delete_published_majors',
			'delete_others_posts' => 'delete_others_majors',
			'edit_private_posts' => 'edit_private_majors',
			'edit_published_posts' => 'edit_published_majors',
			'create_posts'      => 'edit_majors'
		),
		'hierarchical'          => false,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		'has_archive'           => false,
		'rewrite'               => array(
			'slug'  => 'majors',
			'feeds' => false,
			'pages' => true,
		),
		'query_var'             => true,
	) );

}

